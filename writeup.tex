
\documentclass{tufte-handout}

\title{Towards a probabilistic method for predicting player aging}
\author{Cory Frontin\thanks{Graduate student \\ Massachusetts Institute of Technology}}

\usepackage{enumitem}
\usepackage{graphicx}

\begin{document}

\maketitle

\begin{abstract}
  \noindent
  Historically, the so-called delta method has been used to predict how player
  performance is expected to change due to aging from one season to the next. In
  this work, we develop a framework for estimating a player's future-season
  performance with confidence bounds using Gaussian process kriging and an
  extended delta method.
\end{abstract}

\section{Past work on player aging: the delta method}

People have worked on aging curves before\cite{slowinski2010aging,
weinberg2015beginners}. The predominant methodology that has been used to date
is known as the delta method. In the delta method, a season-player statistic of
interest is given by $y_i^{p_k}$, where $y$ is a stastic of interest, e.g.
$wOBA$ or $WAR$ or just as easily $ERA$ or $BA$, $p_k$ is the player to whom the
statistical measure belongs, and $i$ is the player age at the beginning (or end)
of the season of interest. The delta method takes some subset of players, $A_i$
\footnote{This definition of $A_i$ allows for a subset of players for which
there are stastistically significant plate appearances in both age-$(i - 1)$ and
age-$i$ seasons (which is necessary in general), as well as natural extensions
to positional splits.}, and gives the average change between the age $(i - 1)$
and age $i$ season:
\begin{equation}
\label{eq:deltaMethod}
\overline{\Delta y}_{i}= \frac{1}{N_i} \sum_{p_k \in A_i} (y_i^{p_k}
- y_{(i - 1)}^{p_k})
\end{equation}
where $N_i$ is equal to the number of players in $A_i$. In
Figure~\ref{fig:zimmermanAgingCurve}, we can see an example of the aging curve
created by the delta method from a popular FanGraphs post
\cite{zimmerman2013are}. A particularly interesting case is Henry Druschel's
analysis which looks at the aging curve of ''phenoms'' versus their complement
\cite{druschel2015aging}.
\begin{marginfigure}
\includegraphics[width= \linewidth]{img/aging_curve_wrcp.jpg}
\caption{An example of a delta method aging curve with $y= wOBA$ and $A_i$
selected to only include player-seasons in certain eras.}
\label{fig:zimmermanAgingCurve}
\end{marginfigure}

There has been a fair amount of iteration on the delta method. One major
criticism of the delta method is ''survivor bias''. The estimator in
Equation \ref{eq:deltaMethod} is generated from players that made it from year
$i - 1$ to year $i$. This collection of players, crucially, ignores the
existence of players that drop out of the league between years $i - 1$ and $i$.
Authors have long understood and sought to correct this bias
\cite{litchman2016new}.

Nonetheless, the classical delta method does a pretty good job of predicting the
ebb and flow of a generic player's performance entering the $i$-th season.
But for any given player $p_k$ in any given year $i$, any number of variables
can effect the specific case: player injury recovery, nutritional changes,
playoff hangovers, gambling debts, etc. A key question that arises is: how can
we account for the spread of possiblities that a given aging curve might
include?

Modern numerical methods for data analysis point toward a probabilistic
approach, wherein the natural uncertainty of observing, say, a player's ''true
talent'' via a finite number of observations-- in the baseball case, plate
appearances-- are factored into the calculations. Some notable work in this area
includes the use of Markov models for outcomes \cite{polaski2013markov,
melling2017predicting} as well as Bayesian signing risk estimation
\cite{forman2019projecting}. In particular, Sean Forman's work to develop a
framework for projecting signing risk for a player is particularly interesting,
although it notably does not factor player aging into its calculations. This
leaves the question: \emph{is it possible to modernize the delta method in order
to better assess player aging as a spectrum of possibilities, rather than a
generic, deterministic process?} And, if so, \emph{can we leverage such a model
to give probabilistic player-value or team-value assessments?} In this
presentation, I'll explore these two questions.

\section{Investigating the probability densities of player aging}

To start off, we consider Equation~\ref{eq:deltaMethod}. It is observed that
this is the calculation of the mean change from one year to the next. We can,
similarly, calculate the sample standard deviation of each of these steps:
\begin{equation}
\label{eq:deltaMethodStDev}
\sigma_{\Delta y}^{(i)}= \sqrt{\frac{\sum_{p_k \in A_i}
\left(\Delta y_{i}^{p_k} - \overline{\Delta y}_{i}\right)}{N_i - 1}}
\end{equation}
where $\Delta y_{i}^{p_k} \equiv y_{i}^{p_k} - y_{(i - 1)}^{p_k}$. We can assume
that\footnote{I should add an analysis of the distributions of player-seasons
here, and theoretically I could find a better way of sampling them later on.}
that the change in statistic $y$ from year $(i - 1)$ to year $i$ for a given
player $p_k$, $\Delta y_{i}^{p_k}$ is distributed normally with a mean
$\mu_{\Delta y}^{(i)}$ and a standard deviation given by
$\sigma_{\Delta_y}^{(i)}$. Coupling Equation~\ref{eq:deltaMethod} with
Equation~\ref{eq:deltaMethodStDev} gives a probabilistic model for a player
entering his age-$i$ season.

At this point, we can describe the hypothetical behavior of a generic player's
year-to-year changes. But what we're really interested in is how a specific
player ages. A key question is whether or not we can use the probabilistic model
specified above to predict the most likely outcomes for a specific player. In
order to do this, consider the method of Kalman filtering. In Kalman filtering,
a series of noisy observations of an underlying deterministic or Markov
probabilistic underlying process can be used to predict the ''true state'' of
the underlying variable. Consider, for instance, a GPS system, a frequent use
case for Kalman filtering. Say that a person wants to figure out where they are,
at some time $t_i$, which we describe as position $p_i$, which can be thought of
as a vector of latitude and longitude coordinates, for example. The GPS returns
some position $g_i$, a similar vector, but it's noisy. So any given
''observation'' of the GPS coordinates is a normal distribution centered at the
actual but not-directly-observable position $p_i$. In addition, the person is
moving, where the position at any time $t_j$, $p_j$, is only dependent on the
position at the previous time $p_{(j - 1)}$. Kalman filtering allows for a tight
prediction of $p_i$ based on $q_1$, $q_2$, \dots, $q_i$. When the process is
well described by the models, a far tighter prediction of $p_i$ can be made with
this process than can be made on $q_i$ alone, and it will give a distribution of
expected results.

What in the hell does this have to do with baseball? Well, consider in baseball
the notion of ''true talent''. The idea is that a given player's statistics are
only an observation of how good he actually is. If we couple the probabilistic
aging curve above with a suitable estimate of the natural ''noise'' in baseball
statistics, can we take the ideas and assumptions from Kalman filtering to make
better predictions of player aging?

We can start by looking at the assumptions of the Kalman filtering methods:
\begin{itemize}
[noitemsep]
\item a linear state transition model
\item process noise drawn from a zero-mean multivariate normal distribution
\item observation noise drawn from a zero-mean multivariate normal distribution
\end{itemize}
and how they are manifest in the application example of baseball.

\bibliography{writeup}
\bibliographystyle{plainnat}

\end{document}
