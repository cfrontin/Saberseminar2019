
def fit_pdf_MLE(x_samples, pdf= 'gauss'):
  # pdf function
  def logpdf(x, beta, pdf):

    if pdf == 'gauss':
      # unpack parameters
      assert(len(beta) == 2);
      mu= beta[0]; # mu
      var= np.exp(beta[1]); # var= sigma^2

      # the log of the probability (better for numerics)
      logp= -0.5*np.log(2*math.pi*var) - (x - mu)**2/(2*var);

      return logp
    else:
      error("pdf undefined");
      return

      # likelihood calculation
      def calculate_loglikelihood(x_in, beta, pdf):

        # initialize
        logL= 0.0;

        # loop over the samples handed in
        for x in x_in:
          # likelihood of all the samples is the product
          # of the probabilities or the sum of the logprobabilities
          logp= logpdf(x, beta, pdf);

          # add the logprobability to the loglikelihood
          logL= logL + logp;

          # return the log likelihood
          return logL

          # want to maximize likelihood aka minimize negloglikelihood
          def calculate_negloglikelihood(beta, x_in, pdf):

            # calculate the loglikelihood
            logL= calculate_loglikelihood(x_in, beta, pdf);
            logL= -logL;

            # return the newly flipped thing
            return logL;
