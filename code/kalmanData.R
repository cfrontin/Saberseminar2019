
yearLim= 1942

# getPlayerSeasonsWAR <- function(yearLim= -Inf) {
  
  if (file.exists("deltaWAR.txt") && yearLim == -Inf) {
    dfWAR <- read.csv("deltaWAR.txt")
    return(dfWAR)
  }
  
  # grab batting statistics table and look at WAR, birth year, and current year for given players
  dfWAR <- read.csv("war_daily_bat.txt")
  dfWAR <- dfWAR %>% filter(year_ID > yearLim)
  
  # filter for only players with recorded ages, WAR
  dfWAR <- dfWAR %>% 
    filter(age != "NULL") %>%
    filter(WAR != "NULL")
  
  # number of players
  N_players <- length(unique(dfWAR$player_ID))
  
  # number of player-sesasons recorded
  N_playerseasons <- nrow(dfWAR)
  
  # estimate avg. number of years
  N_years_avg <- nrow(dfWAR)/N_players
  
  # what i care about is the total WAR per season and the total number of games per season
  dfWAR <- dplyr::select(dfWAR, "player_ID", "year_ID", "stint_ID", "age", "WAR", "G", "salary")
  dfWAR$age <- as.integer(as.character(dfWAR$age))
  dfWAR$WAR <- as.double(as.character(dfWAR$WAR))
  dfWAR$G <- as.integer(as.character(dfWAR$G))
  dfWAR$salary <- as.numeric(as.character(dfWAR$salary))
  
  # and i don't care about stints so let's group them out
  dfWAR <- summarize(group_by(dfWAR, player_ID, year_ID), age= mean(age), WAR= sum(WAR),
                     G= sum(G), salary= mean(salary))
  
  # make a place to put the last season's WAR and number of games played
  dfWAR$lastWAR <- NA
  dfWAR$lastG <- NA
  
  dfWAR
  
  # # now comes the fun part...
  # # loop over players
  # for (player in sort(unique(dfWAR$player_ID))) {
  # 
  #   stats_player <- filter(dfWAR, player_ID == player)
  #   
  #   # loop over years played by a given player
  #   for (year in sort(unique(stats_player$year_ID))) {
  # 
  #     # grab the previous season WAR if it exists
  #     replacement <- filter(stats_player, year_ID == year - 1)
  # 
  #     # IF the previous season exists, store it
  #     dfWAR[dfWAR$player_ID == player & dfWAR$year_ID == year, ]$lastWAR <-
  #       ifelse(nrow(replacement) == 1, replacement$WAR, NA)
  #     dfWAR[dfWAR$player_ID == player & dfWAR$year_ID == year, ]$lastG <-
  #       ifelse(nrow(replacement) == 1, replacement$G, NA)
  # 
  #   }
  # 
  #   print(paste("updated", player))
  # 
  # }
  # 
  # # now, dump the ones that didn't even exist
  # dfWAR <- dfWAR %>% filter(!is.na(lastWAR))
  # 
  # # calculate the difference in WAR per games played
  # dfWAR <- mutate(dfWAR, WARperG= WAR/G, lastWARperG= lastWAR/lastG)
  # dfWAR$deltaWARperG <- dfWAR$WARperG - dfWAR$lastWARperG
  
  write.csv(dfWAR, file= paste("deltaWAR.txt"), col.names= TRUE)
  
  # ship it!
  return(dfWAR)
  
# # grab batting statistics table and look at OPS (for now) birth year and current year for given players
# stats <- battingStats(Lahman::Batting)
# stats <- stats %>% left_join(Lahman::Master, by="playerID")
# stats <- filter(stats, yearID > yearLim)
# 
# # number of players
# N_players <- length(unique(stats$playerID))
# 
# # estimate avg. number of years
# N_years_avg <- nrow(stats)/N_players
# 
# # filter only for first stints
# stats <- stats %>% filter(stint == 1)
# 
# # filter only for players with recorded birthdates
# stats <- stats %>% filter(!is.na(birthDate))
# 
# stats$birthDate <- as.Date(stats$birthDate)
# stats$seasonStartDate <- paste(stats$yearID, "-04-30", sep="") %>% as.Date
# stats$seasonEndDate <- paste(stats$yearID, "-11-01", sep="") %>% as.Date
# 
# # Function to calculate an exact full number of years between two dates
# year.diff <- function(firstDate, secondDate) {
#   yearsdiff <- year(secondDate) - year(firstDate)
#   monthsdiff <- month(secondDate) - month(firstDate)
#   daysdiff <- day(secondDate) - day(firstDate)
#   
#   if ((monthsdiff < 0) || (monthsdiff == 0 & daysdiff < 0)) {
#     yearsdiff <- yearsdiff - 1
#   }
#   
#   yearsdiff
# }
# 
# # get the age and veteran status of players, filter out ones that don't make sense
# stats$seasonAge <- year.diff(stats$birthDate, stats$seasonStartDate)
# stats$seasonVeterancy <- year.diff(as.Date(stats$debut), stats$seasonEndDate)
# stats <- stats %>% filter(!is.na(seasonVeterancy)) %>% filter(!is.na(seasonAge))
# 
# # prune down to variables of interest
# stats_mini <- stats %>% dplyr::select(playerID, stint, teamID, yearID, OPS, seasonAge, seasonVeterancy, bbrefID)
# 
# # make a place to put the last season's OPS
# stats_mini$lastOPS <- NA
# 
# # loop over players
# for (player in sort(unique(stats_mini$playerID))) {
#   
#   stats_player <- filter(stats_mini, playerID == player)
#   
#   # loop over vererancy status to find previous year's data
#   for (vetStatus in sort(stats_player$seasonVeterancy)) {
#     
#     # grab the previous season OPS if it exists
#     replacement <- filter(stats_player, seasonVeterancy == vetStatus - 1)
#     
#     # IF the previous season is not split AND exists, store it
#     stats_mini[stats_mini$playerID == player & stats_mini$seasonVeterancy == vetStatus, ]$lastOPS <-
#       ifelse(nrow(replacement) == 1, replacement$OPS, NA)
#   }
#   
#   print(paste("updated", player))
#   
# }
# 
# # now, dump the ones that didn't even exist
# stats_mini <- stats_mini %>% filter(!is.na(lastOPS))
# # calculate the difference
# stats_mini$deltaOPS <- stats_mini$OPS - stats_mini$lastOPS
# stats_mini <- stats_mini %>% filter(!is.na(deltaOPS))
# 
# return(stats_mini)

# }

# getPlayerSeasonsWAR(1942)
