
import csv

filename_in= "deltaWAR.txt"
filename_out= "deltaWAR2.txt"

with open(filename_in, "r") as f:
  reader= csv.reader(f)
  data= list(reader)

nRows= len(data)
print "nRows %d" % nRows

data[0].append("deltaWARperGame")
data[0].append("pChangeWARperGame")

print len(data[0])

for idx, line in enumerate(data[1:]):
  bbrefID= str(line[1])
  yearID= int(line[2])
  age= int(line[3])
  WAR= float(line[4])
  games= float(line[5])
  changed= False
    
  for otherline in data[1:]:
    if otherline[1] == bbrefID and int(otherline[2]) == yearID - 1:
      lastWAR= float(otherline[4])
      lastGames= float(otherline[5])
      WARperGame= WAR/games
      lastWARperGame= lastWAR/lastGames
      
      data[idx][7]= lastWAR
      data[idx][8]= lastGames
      data[idx].append(WARperGame - lastWARperGame)
      if lastWARperGame == 0:
        data[idx].append(0)
      else:
        data[idx].append((WARperGame - lastWARperGame)/lastWARperGame)
      changed= True
      print str(idx) + " of " + str(nRows) + " changed!"
  
  if not changed:
    data[idx].append('NA')
    data[idx].append('NA')

f= open(filename_out, "w")
writer= csv.writer(f)
writer.writerows(data)

f.close()
