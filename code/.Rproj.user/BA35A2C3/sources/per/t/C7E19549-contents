---
title: "Simple WAR Kalman Filtering Demo"
output: pdf_document
---

# A probabilistic approach for the "delta-method" for projecting future player performance

I'm interested in seeing how a probabilistic "delta-method" player aging curve model can leverage the Kalman filtering technique to make guesses and propose confidence bounds for future player performance. In order to do so, we first use the Lahman database to load the change in WAR per game since the last season and player age for every player-season since baseball was integrated.

```{r, results='hide', message= FALSE}
# includes
library(dplyr)
library(ggplot2)
library(Lahman)
library(lubridate)
library(MASS)

# load data
dfWAR <- read.csv("deltaWAR2.txt")
dfWAR <- filter(dfWAR, !is.na(deltaWARperGame)) %>%
  dplyr::select(player_ID, year_ID, age, WAR, G, salary, lastWAR, lastG, deltaWARperGame) %>%
  filter(year_ID >= 1941) %>%
  filter(G > 100)

```

Given this data, we assume a model that for player performance in year $i$, $y_i$: that $y_i$ is a function of player age $t_i$ given by:
$$y_{i + 1}= y_i + \Delta Y(t_i)$$
where $\Delta Y(t_i)$ is a continuous random variable that represents the probability distribution of performance deltas. This is assumed to be independent of everything except for player age $t_i$. For starters, we will assume that $\Delta Y(t_i)$ is a normal random variable:
$$\Delta Y(t_i) \sim \mathcal{N}(\mu_i, \sigma_i^2)$$
where $\mu_i$ and $\sigma_i^2$ are the mean and variance, respectively, of the random normal distribution of changes in player performance $\Delta y$ that are found in the data. We can get the set of $(\mu_i, \sigma_i^2)$ from the Lahman database by fitting the histograms of player performance:

```{r}
# grab stats grouped by the age of the player in the current season
statistics <- dfWAR %>% group_by(age) %>% summarize(N= n(), mean= mean(deltaWARperGame), sd= sd(deltaWARperGame))

# filter off the ages for which there are not many samples
statistics <- statistics %>% filter(N > 100)

# create space to store distribution vars w/ the number of ages we want
N_age <- nrow(statistics)
mu_age <- numeric(N_age)
var_age <- numeric(N_age)
stderr_age <- numeric(N_age)

# loop over "ages"
for (i in 1:N_age) {
  
  # grab the age
  ageTgt <- statistics$age[i]
  
  # cut down to the specific age we're about
  working_group <- dfWAR %>% filter(age == ageTgt)
  
  mu_age[i] <- mean(working_group$deltaWARperGame)
  var_age[i] <- var(working_group$deltaWARperGame)
  stderr_age[i] <- sqrt(var_age[i])
  
  # # get the mean and standard deviation for this age
  # distro <- fitdistr(working_group$deltaWARperGame, "normal")
  # 
  # # set in the things
  # mu_age[i] <- distro$estimate["mean"]
  # var_age[i] <- distro$estimate["sd"]
  
}

ages_plot <- data.frame(age= statistics$age, deltaWAR= 162*mu_age, variance= 162*var_age)
scatter_plot <- data.frame(age= dfWAR$age, deltaWAR= 162*dfWAR$deltaWARperGame)
ggplot(data= ages_plot, aes(x= age, y= deltaWAR)) +
  geom_line() +
  geom_ribbon(data= ages_plot,
              aes(ymin= 162*(mu_age - 2*stderr_age), ymax= 162*(mu_age + 2*stderr_age)),
              alpha= 0.3) +
  geom_point(data= scatter_plot)
```
Now, we can describe the "most probable" change in WAR from one season to the next, $\mu_{\Delta (WAR/GP)}$, *as well as* the associated variance $\sigma_{\Delta (WAR/GP)}^2$! In other words, we can describe the probability distribution of the change in $(WAR/GP)$ from one season to the next.

Let's look at slices, starting with the age 25 season:
```{r}
# loop over "ages"
for (i in 1:N_age) {
  
  # grab the age
  ageTgt <- statistics$age[i]
  
  # cut down to the specific age we're about
  working_group <- dfWAR %>% filter(age == ageTgt)
  
  
  
}
```


Chris Davis is my personal whipping boy. So let's look at his WAR sequence.
```{r}
CDstats <- dfWAR %>% filter(player_ID == "davisch02")
ggplot(CDstats, aes(x= year_ID, y= WAR/G)) + geom_line()
```
I'm not a doorknob. I can see that he's been both very good and very bad, from an WAR standpoint. We can also look at his $\Delta WAR$ numbers:
```{r}
ggplot(CDstats, aes(x= year_ID, y= deltaWARperGame)) + geom_line()
```
Some years, he goes up. Some years he goes down. Some years he goes *way* down (2013). Now, we have a sequence of data of his $WAR$ numbers, but we also have a **model** for how we expected the generic player's performance to change!

Now we make the cricial ''true-talent'' assumption. Chris Davis- or any player for that matter- has a true level of talent. Likewise, there is a "true $WAR$" that he would achieve if the season included an infinite number of at-bats. We'll call that $y \equiv WAR_\mathrm{true}$. Fortunately, real baseball is finite and, therefore, unpredictable. Likewise we witness a player's *actual* $WAR$, which we denote with $z$. Thus, $z$ is a "noisy estimate" of $y$:
$$z \sim \mathcal{N}(y, \sigma_{WAR}^2)$$
We've assumed that it's drawn from the normal distribution centered at the true talent level, with some variance $\sigma_{WAR}^2$.
